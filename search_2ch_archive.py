#!/usr/bin/python3
import requests, re
#import sys

def test_link_text(text):
    text = text.upper()
    words = (
        'ЭРДОГАН',
        'ТРИУМФАЛЬНО',
        'ВОЕННОЕ ВМЕШАТЕЛЬСТВО',
        'ИНДЕКСИРОВАН БЕЗУКОРИЗНЕННО',
        'ЗАКАДЫЧНО[ПАРРОМА]',
        'РОБОТИЧЕСКИ',
        'ЫТРЭЧ',
        'ЧЕЧЕВИЧНАЯ ПОХЛЕБКА',
        'ЗАКОННО!',
        'АКРОБАТИЧЕСКОЕ',
        'МЕСТА(!)',
        'СУХОГРУЗ',
        'ГАЗ-53',
        'ОТКЛАДЫВАЕТСЯ ЛИНЕЙНО',
        'ДЕЖУРНЫЕ С ВЫШЕСТОЯЩИМИ',
        'Х О Р Т И Ц А',
        'ЯРОСЛАВСКАЯ ГУБЕРНИЯ',
        'ПРИСУТСТВИЕ ВОЕННОЕ',
        'ЩУКА В МЕШКЕ',
        'ИМЕНИЯ ВЫШЕСТОЯЩИХ',
        'ФЕХТОВАЛЬНЫЕ НАВЫКИ',
        'ОТМЕЧЕНО!',
        'ЛИНЕЙНО',
        'УКДВРК',
        'КАЗНЬ ПО',
        'Ь - ЕГО ВЕЛИЧЕСТВО',
        'ЙОДИНОЛОВЫЙ',
        'ОН ПРИГЛЯДЫВАЛСЯ ТАК ПРИСТАЛЬНО',
        'ЭКРАНИРОВАНИЕ ПО ИНСТРУКЦИИ',
        'ОДОБРЕНО!',
        'ПОБЕДНО ШЕСТВУЕМ',
        'МАШИНА И БУТЫЛКА ВИСКИ',
        'БЕРКУТ'
    )
    for w in words:
        if w in text:
            return True
    return False

def find_triumfalno():
    page = 0
    board = "b"
    while page <= 898:
        print("fetching page %d" % page)
        url = "https://2ch.hk/%s/arch/%d.html" % (board, page)
        r = requests.get(url)
        for a in re.finditer(r'<a href="(/'+board+'/arch/[\d-]+/res/\d+\.html)">(.*?)</a>', r.text, flags=re.I|re.M):
            link_href = a.group(1)
            full_href = 'https://2ch.hk' + link_href
            link_text = a.group(2)
            if test_link_text(link_text):
                print("%s => %s" % (full_href, link_text))
        page += 1

def find_a2ch():
#    with open('/tmp/text.txt') as f:
#        known = f.read().strip().split(' ')
#    known = tuple(map(lambda x: re.sub(r'^(.*?)/([\d]+)\.html$', '\\2', x), known))
    dates = (
        '11/15',
        '11/16',
        '11/17',
        '11/18',
        '11/19',
        '11/20',
        '11/21',
        '11/22',
        '11/23',
        '11/24',
        '11/25',
        '11/26',
        '11/27',
        '11/28',
        '11/29',
        '11/30',
        '12/01',
        '12/02',
        '12/03',
        '12/04',
        '12/05',
        '12/06',
        '12/07',
        '12/08',
    )
    for date in dates:
        print("fetching %s..." % date)
        url = 'http://a2ch.ru/2016/' + date + '/'
        r = requests.get(url)
        for a in re.finditer(r'<a href="(/2016/(?:.*?)-(\d+)\.html)">(.*?)</a>', r.text, flags=re.I|re.M):
            full_href = 'http://a2ch.ru' + a.group(1)
            link_text = a.group(3)
            thread_id = a.group(2)
            if test_link_text(link_text):#; and thread_id not in known:
                print("%s => %s" % (full_href, link_text))

if __name__ == '__main__':
    find_triumfalno()
    #find_a2ch()
