#!/usr/bin/env python3

import argparse
import sys
import os
from pprint import pprint

try:
    from termcolor import cprint
    colors_supported = True
except ImportError:
    colors_supported = False

from data_lib import load_data, decode_auto, clean_string


def print_colored(s, color, fallback_prefix=''):
    if colors_supported:
        cprint(s, color)
    else:
        print(fallback_prefix + s)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--decode', action='store_true')
    parser.add_argument('--stats', action='store_true')
    parser.add_argument('--decode-string')
    parser.add_argument('--decode-file')
    parser.add_argument('--with-junk', action='store_true')
    parser.add_argument('--is-url', action='store_true')
    parser.add_argument('--type', type=int, choices=[1, 2, 3, 4], default=1)
    parser.add_argument('--reverse-decoded', action='store_true')

    args = parser.parse_args()
    data = load_data('date', sort_reverse=True)

    if args.decode:
        # filter by type
        if args.type == 2:
            data = list(filter(lambda i: 'type' in i and i['type'] == 2, data))
        else:
            data = list(filter(lambda i: 'type' not in i, data))   

        for obj in data:
            text = obj['text']
            text_decoded = decode_auto(text,
                                      args.type,
                                      remove_junk=(not args.with_junk),
                                      reverse_decoded=args.reverse_decoded)
         
            # print all information
            print(obj['text'])
            print_colored(clean_string(text, remove_junk=(not args.with_junk)), 'green', fallback_prefix='[CLEANED] ')
            print_colored(text_decoded, 'cyan', fallback_prefix='[DECODED] ')
            
            if 'pic' in obj:
                pic = obj['pic'] if isinstance(obj['pic'], list) else [obj['pic']]
                print_colored(', '.join(pic), 'red', fallback_prefix='[PICS] ')
            if 'link' in obj:
                print_colored(obj['link'], 'red', fallback_prefix='[LINK] ')

            print("\n")

    elif args.decode_string or args.decode_file:
        if args.decode_string:
            source = args.decode_string
        else: 
            with open(args.decode_file, 'r') as f:
                source = f.read()

        text_decoded = decode_auto(source,
                                   args.type,
                                   remove_junk=(not args.with_junk),
                                   reverse_decoded=args.reverse_decoded)
        
        # print
        print_colored(text_decoded, 'cyan', fallback_prefix='[DECODED] ')
            
    elif args.stats:
        count = len(data)
        print("Total texts: %s" % count)

if __name__ == '__main__':
    sys.exit(main())
