#!/usr/bin/python3.4
import sys
import os
import shutil
import argparse
import subprocess
from pprint import pprint
from data_lib import load_data, decode_auto

CWD = os.path.dirname(os.path.realpath(__file__))
MD_START = """
## Описание

Этот файл автоматически генерируется скриптом `gen_md.py` из данных в файле `data.json`. Чтобы открыть картинку в полном размере, кликни на нее.

Для первого типа *дата* не всегда означает, что именно в этот день шифровка появилась в первый раз, потому что многие из них постились по многу раз и не всегда легко определить самую первую дату. Но админ этого репозитория вручную перебрал сотню тредов в архиве, и скорее всего даты соответствуют действительности.

*Тип 1* - это где "ВОЕННОЕ ВМЕШАТЕЛЬСТВО ЭРДОГАН ТРИУМФАЛЬНО" и тд. Для расшифровки нужно брать первую букву каждого слова, пропуская отдельные слова и словосочетания. См. алгоритм в `data_lib.py`.

*Тип 2* - шифровка составлена из предложений. Для расшифровки нужно брать первую букву второго слова каждого предложения, это еще проще чем *тип 1*. См. алгоритм в `data_lib.py`.

*Тип 3* - шифровка составлена из предложений. Каждое предложение - одна буква. Для расшифровки нужно удалить все пробелы и использовать скрипт `analyze_new.py` для частотного анализа текста. Надежного алгоритма расшифровки пока нет. Подробности в треде https://2ch.pm/sn/res/408273.html#412342. После нового года, видимо, шифр изменился и стал устойчив к частотному анализу.

*Тип 4* - новый шифр, появившийся 13 февраля. Похож на язык ктулху.

*Тип 5* - пост от 22 марта.

*Тип 2char_rot-3 - примерно с середины января до текущего момента (середина мая). Нужно разбить текст на предложения, удалить пробелы, из каждого предложения взять вторую букву и применить шифр Цезаря на 3 буквы назад (на самом деле, на 2).*

## Известные шифровки

"""

def resize(in_path, out_path):
    subprocess.call(['convert', in_path, '-resize', '250', out_path])

def gen_previews():
    img_dir = os.path.join(CWD, 'img')
    img_previews_dir = os.path.join(CWD, 'img_previews')
    if os.path.exists(img_previews_dir):
        shutil.rmtree(img_previews_dir)
    
    os.makedirs(img_previews_dir)
    for img in os.listdir(img_dir):
        img_path = os.path.join(img_dir, img)
        img_preview_path = os.path.join(img_previews_dir, img)

        resize(img_path, img_preview_path)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--with-previews', action='store_true')
    args = parser.parse_args()

    data = load_data(sort='date')

    if args.with_previews:
        print("Generating previews (don't forget to git add them)...")
        gen_previews()

    print("Generating page...")
    buf = []

    for post in data:
        cipher_type = post['type'] if 'type' in post else '1'
        if cipher_type in ('1', '2', '2char_rot-3'):
            decoded_text = decode_auto(post['text'], cipher_type)
        elif cipher_type == '3':
            decoded_text = post['decoded']
        else:
            decoded_text = '?'

        post_buf = ''
        post_buf += '**Дата**: %s\n\n' % post['date']

        if 'pic' in post and post['pic']:
            # make sure it is a list
            pic = post['pic'] if isinstance(post['pic'], list) else [post['pic']]

            pic_buf = []
            for p in pic:
                pic_buf.append('[![](./img_previews/%s)](./img/%s)' % (p, p))
            post_buf += '**Пикрилейтед:**\n\n%s\n\n' % ' '.join(pic_buf)

        if 'link' in post:
            # make sure it is a list
            link = post['link'] if isinstance(post['link'], list) else [post['link']]

            link_buf = []
            for l in link:
                link_buf.append('[%s](%s)' % (l, l))
            post_buf += '**Ссылки:** %s\n\n' % ', '.join(link_buf)
            
        if 'file' in post:
            # make sure it is a list
            file = post['file'] if isinstance(post['file'], list) else [post['file']]

            file_buf = []
            for f in file:
                file_buf.append('[%s](./files/%s)' % (f, f))
            post_buf += '**Файлы:** %s\n\n' % ', '.join(file_buf)

        if 'source' in post:
            post_buf += '**Источник:** %s' % post['source']
            if 'source_link' in post:
                post_buf += ', [%s](%s)\n\n' % (post['source_link'], post['source_link'])
            else:
                post_buf += '\n\n'

        post_buf += '**Шифровка (тип %s)**:\n> %s\n\n' % (cipher_type, post['text'].replace("\n", "\n>\n"))
        post_buf += '**Расшифровка:**\n> %s' % decoded_text
        
        buf.append(post_buf)

    md = MD_START
    md += "\n\n\n\n---------\n\n\n\n".join(buf)
    with open(os.path.join(CWD, 'data.md'), 'w') as f:
        f.write(md)

    print("Done.")

if __name__ == '__main__':
    sys.exit(main())
