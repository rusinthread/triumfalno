#!/usr/bin/python3
import re
import operator
import itertools
import sys
import argparse
from pprint import pprint

def load_text(n):
    with open('new/text' + str(n) + '_orig') as f:
        text = f.read()

    lines = re.split(r'[\?\.\!]+', text) 
    lines = list(map(lambda l: l.strip(), lines))
    lines = list(map(lambda s: s.replace(' ', '').replace('-', ''), lines))

    return "\n".join(lines).strip()

def spaceitout(string,amount):
    amountint= int(amount)
    pile= ""
    for char in string:
        pile= pile + char + " "*amount
    return pile.strip()

def letter_pos(letter):
    if letter in predefined_table:
        return predefined_table[letter]
    else:
        letter_table = table[letter]
        if len(letter_table) > 0:
            return letter_table[0][0]
        else:
            return None

def die(t):
    pprint(t)
    sys.exit()

def dump_lines():
    i = 0
    for l in lines:
        print('%d ' % i, end='')
        print(l)
        i += 1
    sys.exit()

alphabet = 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'
a = spaceitout(alphabet, 1)
a = a.replace(' ', ',')
#print(a)
#sys.exit()

text = load_text(10)
#text += load_text(1) + "\n"
#text += load_text(2) + "\n"
#text += load_text(3) + "\n"
#text += load_text(4)
text = text.upper()

predefined_table = {
    'М': 8,
    'А': 5,
    'Р': 3,
    'Ш': 1,
    'Щ': 1,
    'И': 1,
    'У': 2,
    'Ю': 10,
    'Й': 5,
    'Л': 2,
    'Ц': 2,
    'О': 4,
    'Д': 8,
    'Т': 9,
    'П': 5,
    'Э': 3,
    'Х': 5
}

table = {}
lines = text.split("\n")

#dump_lines()

for a in alphabet:
    table[a] = {}
    for line in lines:
        indexes = [m.start() for m in re.finditer(a, line)]
        for index in indexes:
            index += 1
            if index in table[a]:
                table[a][index] += 1
            else:
                table[a][index] = 1

for a, t in table.items():
    ts = sorted(t.items(), key=operator.itemgetter(1), reverse=True)
    table[a] = ts

#die(table)

variants = []
for line in lines:
    valid = []
    for a in table:
        if True:
            index = letter_pos(a)
            if index == None:
                continue
            
            try:
                if line[index-1] == a and a not in valid:
                    valid.append(a)
            except IndexError:
                continue
        
        else:
            letter_table = table[a]
            if not len(letter_table):
                continue

            for i in range(2):
                if i > len(letter_table)-1:
                    continue

                if a == 'Щ':
                    index = 1
                else:
                    index = letter_table[i][0]
                try:
                    if line[index-1] == a and a not in valid:
                        valid.append(a)
                except IndexError:
                    continue

    if len(valid) == 0:
        valid = ['?']
    variants.append(valid)
    #print('('+''.join(valid)+')')

variants = list(filter(lambda a: len(a), variants))
#variants = list(map(lambda l: sorted(l), variants))
#die(variants)

variants = variants[:7]
res = list(itertools.product(*variants))
for r in res:
    if r[0] == 'Ь': continue
    print(''.join(r))
